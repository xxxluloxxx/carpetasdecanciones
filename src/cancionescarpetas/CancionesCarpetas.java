/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cancionescarpetas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 *
 * @author Paul Arias
 */
public class CancionesCarpetas {

    public void LeerFichero() {
        try {
            FileReader fr = new FileReader("archivos.csv");
            BufferedReader bf = new BufferedReader(fr);

            String sCadena = "";
            while ((sCadena = bf.readLine()) != null) {
                String[] valores = sCadena.split(",");
                for (int i = 0; i < valores.length; i++) {
                    //CrearCarpeta(valores[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CrearCarpeta(String name, String rutaDestino) {
        File directorio = new File(rutaDestino+"\\" + name.trim());
        directorio.mkdir();
    }

    public void ListarArchivos(String rutaOrigen, String rutaDestino) {
        String sDirectorio = rutaOrigen;
        File f = new File(sDirectorio);
        int mitad=0;
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (int x = 0; x < ficheros.length; x++) {
                for (int i = 0; i < ficheros[x].getName().length(); i++) {
                    if(ficheros[x].getName().substring(i,i+1).equals("-")){
                        mitad=i;
                        break;
                    }
                }
                CrearCarpeta(ficheros[x].getName().substring(mitad+1,ficheros[x].getName().length()-4), rutaDestino);
                MoverArchivos(rutaOrigen, rutaDestino, ficheros[x].getName().substring(mitad+1,ficheros[x].getName().length()-4).trim(), ficheros[x].getName());
            }
        } else {
            System.out.println("No existe el directorio");
        }

    }
    
    public void MoverArchivos(String rutaOrigen, String rutaDestino,String rutaArtista, String nombre){
        Path origenPath = FileSystems.getDefault().getPath(rutaOrigen+"\\"+nombre);
        Path destinoPath = FileSystems.getDefault().getPath(rutaDestino+"\\"+rutaArtista+"\\"+nombre);

        try {
            Files.move(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here

        CancionesCarpetas c = new CancionesCarpetas();
        //c.LeerFichero();
        String rutaOrigen="C:\\Users\\Paul Arias\\Desktop\\Origen";
        String rutaDestino="C:\\Users\\Paul Arias\\Desktop\\Destino";
        
        c.ListarArchivos(rutaOrigen,rutaDestino);
    }

}
